# Getting Started with the Leafbuyer Texting API

This guide will help you get started with sending text campaigns using the Leafbuyer Texting API. We'll cover the authentication mechanism, a high-level overview of GraphQL, and sample use cases. A partial API reference is included at the end of this document. The full API schema can be obtained directly from the API server using GraphQL's schema retrieval facility.

## Table of Contents
- [Introduction](#introduction)
- [Texting API Concepts](#texting-api-concepts)
- [Ad-Hoc Messaging API Concepts](#ad-hoc-messaging-api-concepts)
- [Authentication](#authentication)
- [GraphQL Overview](#graphql-overview)
- [Ad-Hoc Messaging Reference](#ad-hoc-messaging-reference)
  - [Sending Ad-Hoc Messages](#sending-ad-hoc-messages)
  - [Preparing Images for MMS messages](#preparing-images-for-mms-messages)
  - [Sample Use Cases](#ad-hoc-messaging-sample-use-cases)
    - [Sending MMS Messages](#sending-an-mms-message)
  - [Add-Hoc Messaging Webhooks](#ad-hoc-messaging-webhooks)
- [Texting API Reference](#texting-api-reference)
  - [Sample Use Cases](#texting-api-sample-use-cases)
    - [Creating Customers](#creating-customers)
    - [Creating Campaigns](#creating-campaigns)
- [API Reference](#api-reference)

## Introduction

The Leafbuyer Texting API allows third-party developers to integrate SMS and MMS messaging capabilities into their platforms. You can use this API to create and manage customers (ie. recipients), and to schedule text message campaigns that target them.

## Texting API Concepts

Before you start using the API, it’s important to understand some key concepts:

### Tenants

A tenant represents your account in the API. Billing for your text campaigns is done at the tenant level.

### Locations

Locations are subdivisions within a tenant. They represent physical or logical places that customers are associated with. A tenant can have one or more locations.

### Customers (Text Recipients)

Customers are individuals who can receive text messages. Each customer is associated with one or more locations.

### Text Campaigns

Campaigns are collections of text messages that are sent to customers. To send a campaign, you must specify its parameters wich include the location or locations it targets, a message or image, an execution time, and a number of other optional parameters.

### Workflow

1. **Create Customers:** Before you can send a campaign, you need to create customers within your account locations.
2. **Create Campaign:** After creating customers, you can create a campaign and specify its target location(s).

### Entity IDs

Your tenant, its location and the customers associated with those locations are all uniquely identified using Universally Unique Identifiers (UUIDs). In order to obtain the IDs of various entities, helper endpoints are available that allow listing the locations configured for your account, the customers associated with a location, etc. 

## Ad-Hoc Messaging API Concepts

In contrast to the Texting API, the Ad-Hoc Messaging API is a low-level API that allows sending SMS and MMS messages to a phone number, without involving concepts such as a Customer, Locations etc.

### Safeguards

As such, this API does not include a number of safeguards implemented by the Texting API which ensure regulatory compliance and improve deliverability of messages. It is the responsibility of the API user to handle compliance and deliverability concerns such as:
  - Recipient TCPA and opt-out management
  - Keywords banned by carriers
  - Number and nature of included URLs
  - File size restrictions of MMS media
  - Content restrictions on MMS media files
  - etc.

### Webhooks

A webhook mechanism is used to inform the API user of message delivery status updates and message replies sent by recipients.


## Authentication

Our API uses OAuth 2.0 Client Credentials Grant Flow for authentication ([OAuth 2.0 RFC 6749, section 4.4](https://datatracker.ietf.org/doc/html/rfc6749#section-4.4)). This section explains how to obtain an access token to authenticate your API requests.

### Step 1: Obtain Client Credentials

You will need the following credentials:
- Client ID
- Client Secret

These credentials will be provided once your API access is configured by the Leafbuyer staff.

### Step 2: Request an Access Token

To get an access token, make a POST request to the OAuth token endpoint, making sure to select the correct URL depending on the environment.

- staging and development: `https://stage-lbmlp.us.auth0.com/oauth/token`

- Production: `https://leafbuyer.auth0.com/oauth/token`


```
POST https://leafbuyer.auth0.com/oauth/token
Content-Type: application/json

{
  "grant_type": "client_credentials",
  "client_id": "YOUR_CLIENT_ID",
  "client_secret": "YOUR_CLIENT_SECRET",
  "audience": "api"
}
```

An example request using cURL would look as follows:

```
curl --request POST \
  --url 'https://leafbuyer.auth0.com/oauth/token' \
  --header 'content-type: application/x-www-form-urlencoded' \
  --data grant_type=client_credentials \
  --data client_id=YOUR_CLIENT_ID \
  --data client_secret=YOUR_CLIENT_SECRET -
  --data audience=api
```

An example response returned by the server would be:

```
{"access_token":"eyJhbGYHZCqqH92UHK7F9sQK...<truncated>...WSbGz0mW-vVepH2GXhXQt8IwwONFg","expires_in":86400,"token_type":"Bearer"}
```


### Step 3: Use the Access Token

Include the access token in the Authorization header of your API requests:

```
Authorization: Bearer YOUR_ACCESS_TOKEN
```

The API endpoints to use are:

- staging and development: `https://api.ingress.stage.lbmlp.dev.leafbuyerloyalty.com`

- Production: `https://api.leafbuyerloyalty.com`


Using cURL again, the following request would query your account details:
```
curl --request POST \
  --url https://api.leafbuyerloyalty.com/ \
  --header 'Authorization: Bearer eyJhbGYHZCqqH92UHK7F9sQK...<truncated>...WSbGz0mW-vVepH2GXhXQt8IwwONFg' \
  --header 'Content-Type: application/json' \
  --data '{"query":"query tenant { tenant { id, name } }","operationName":"tenant","variables":{}}'
```

And a sample response would be:
```
{"data":{"tenant":{"id":"5a7251b0-a78f-4ff5-9de6-bc967867a73d","name":"My Platform Name"}}}
```

## GraphQL Overview

Our API uses GraphQL for querying and mutating data. GraphQL allows you to specify exactly what data you need, reducing the amount of data transferred over the network and improving performance.

### Making a GraphQL Request

GraphQL requests are made to the following endpoint:

```
POST https://api.leafbuyerloyalty.com
Content-Type: application/json

{
  "query": "YOUR_GRAPHQL_QUERY"
}
```

Please refer to the official GraphQL documentation for more in depth information on the GraphQL concepts, syntax and query examples.

# Ad-Hoc Messaging Reference

## Sending Ad-Hoc Messages

### sendAdHocMessage Mutation

Sends an ad-hoc message to a specified phone number.

**Input:**

```graphql
input SendAdHocMessageInput {
  phoneNumber: String!
  messageBody: String
  imageId: ID
  customMetadata: String
}
```

Where:

- **phoneNumber**: Recipient phone number in E.164 format. (String!)
- **messageBody**: Content of the text message. (String)
- **imageId**: ID of the image to use for MMS. (ID)
- **customMetadata**: Metadata string returned with message status update webhooks. Maximum length of 256 characters. (String)

While both **messageBody** and **imageId** are optional parameters, at least one of them must be supplied with the mutation.
Specifying an **imageId** results in the message being sent as an MMS message.
Please refer to the Sending MMS Use case for details on how to obtain an image ID.

**Mutation:**

```graphql
mutation {
  sendAdHocMessage(input: SendAdHocMessageInput!): SendAdHocMessageResult
}
```

**Example Request for SMS:**

```graphql
mutation {
  sendAdHocMessage(input: {
    phoneNumber: "+1234567890",
    messageBody: "This is an SMS test message"
  }) {
    status
    dispatchId
  }
}
```

**Example Request for MMS:**

```graphql
mutation {
  sendAdHocMessage(input: {
    phoneNumber: "+1234567890",
    messageBody: "This is an MMS test message"
    imageId: "c43b3b1d-2174-4ab8-bc97-4f547b286751"
  }) {
    status
    dispatchId
  }
}
```

**SendAdHocMessageResult**

The result of sending an ad-hoc message.

- **status**: The status of the message sending operation. (String!)
- **dispatchId**: The dispatch ID of the sent message. This will be included with all status webhooks relative to this message. (String)

**Example Response**

```json
{
  "data": {
    "sendAdHocMessage": {
      "status": "QUEUED",
      "dispatchId": "d65445ad-0bcc-44b4-b894-2cecfb7354c1"
    }
  }
}
```

## Preparing Images for MMS messages

### prepareCampaignImageUploadForSubjectLocation Mutation

Returns an image ID and signed URL that is used to upload an image to S3. The image ID is subsequently used to specify the media file for an MMS message.

**Type:**

```graphql
type CampaignImageUploadDetails {
  signedUploadUrl: String!
  imageId: String!
}
```

**Mutation:**

```graphql
mutation {
  prepareCampaignImageUploadForSubjectLocation: CampaignImageUploadDetails
}
```

**Example Request:**

```graphql
mutation {
  prepareCampaignImageUploadForSubjectLocation {
    signedUploadUrl
    imageId
  }
}
```

**Example Response:**

```json
{
  "data": {
    "prepareCampaignImageUploadForSubjectLocation": {
      "signedUploadUrl": "https://s3.amazonaws.com/bucket/upload_url",
      "imageId": "IMAGE_ID"
    }
  }
}
```

## <a id="ad-hoc-messaging-sample-use-cases"></a> Ad-Hoc Messaging Sample Use cases

### Sending an MMS message

In order to send an MMS message, you need to follow these three steps:

1. Invoke the `prepareCampaignImageUploadForSubjectLocation` mutation to obtain an image ID and a signed AWS S3 upload URL.
2. Upload your image to AWS S3 using the signed upload URL.
3. Invoke the `sendAdHocMessage` mutation, passing the image ID obtained in step 1 as a parameter.

**Important notes:** 
- Uploaded images must have a maximum file size of **1MB**.
- The signed upload URLs expire after a few minutes. If an upload URL expires before being used, a new URL and image ID must be obtained by repeating step 1 above.

An image ID can be reused for sending multiple messages with the same media file.

The API offers an automatic image resizing features. This feature is still being documented.

The following section outlines the three steps in more detail, giving example queries for each step.


#### Step 1: Obtain an Image ID and Signed S3 Upload URL

Invoke the `prepareCampaignImageUploadForSubjectLocation` mutation to get the image ID and the signed URL.

**GraphQL Request:**

```graphql
mutation {
  prepareCampaignImageUploadForSubjectLocation {
    signedUploadUrl
    imageId
  }
}
```

**Example Response:**

```json
{
  "data": {
    "prepareCampaignImageUploadForSubjectLocation": {
      "signedUploadUrl": "https://s3.amazonaws.com/bucket/upload_url",
      "imageId": "IMAGE_ID"
    }
  }
}
```

#### Step 2: Upload Your Image to S3

Use the signed URL to upload your image to S3.

**Example CURL Command:**

```bash
curl -X PUT "https://s3.amazonaws.com/bucket/upload_url" \
     -H "Content-Type: image/jpeg" \
     --data-binary "@path_to_your_image.jpg"
```

#### Step 3: Send an MMS Message

Invoke the `sendAdHocMessage` mutation, including the image ID obtained in Step 1.

**GraphQL Request:**

```graphql
mutation {
  sendAdHocMessage(input: {
    phoneNumber: "+1234567890",
    messageBody: "This is an MMS message",
    imageId: "IMAGE_ID"
  }) {
    status
    dispatchId
  }
}
```

**Example Response:**

```json
{
  "data": {
    "sendAdHocMessage": {
      "status": "QUEUED",
      "dispatchId": "DISPATCH_ID"
    }
  }
}
```

## Ad-Hoc Messaging Webhooks

The SMS Texting API supports webhooks to notify your application of various events related to message status updates and incoming messages. This section details the structure and usage of these webhooks.

### Webhook Delivery

Webhooks are sent as POST requests and follow an "at-least-once" delivery approach. This means that the webhook recipient must be able to handle the webhooks in an idempotent manner, ensuring that repeated processing of the same webhook event does not result in duplicate operations or unintended side effects.

### Webhook Types

There are two types of webhooks sent by the Ad-Hoc Messaging API:

1. **Message Status Update**: Provides updates on the transport and delivery status of messages sent through the API.
2. **Incoming Message Notification**: Notifies when a new message is received.

### Webhook Payload

The webhook payload is sent as JSON and is derived from the following Golang struct:

```json
{
  "event_type": "string",
  "occurred_at": "string",
  "recipient_phone_number": "string",
  "sending_phone_number": "string",
  "message_id": "string",
  "message_status": "string",
  "message_status_details": "string",
  "custom_metadata": "string",
  "incoming_message_body": "string"
}
```

#### Fields Description

- **event_type**: The type of the event. Can be one of `MESSAGE_STATUS` or `INCOMING_MESSAGE`.
- **occurred_at**: The timestamp when the event occurred.
- **recipient_phone_number**: The phone number of the message recipient.
- **sending_phone_number**: The phone number from which the message was sent.
- **message_id**: The unique identifier of the message (for message status update events only).
- **message_status**: The status of the message (for message status update events only).
- **message_status_details**: Additional details about the message status (for message status update events only).
- **custom_metadata**: Any custom metadata associated with the message (for message status update events only).
- **incoming_message_body**: The body of the incoming message (for incoming message events only).

Please note that in the case of an incoming message, the `recipient_phone_number` is a Leafbuyer API phone number, and the `sending_phone_number` is the phone number of the customer/individual who sent a message.

#### Event Type Values

The `EventType` field in the payload can have one of the following values:

- **MESSAGE_STATUS**: Indicates a message status update.
- **INCOMING_MESSAGE**: Indicates an incoming message notification.


#### Message Status Values

The `message_status` field in the payload can have one of the following values:

The following statutes are intermediary statuses, or statuses indicating a successful delivery:

- **QUEUED**: The message has been queued and is waiting to be sent.
- **SENDING**: The message is in the process of being sent to the carrier.
- **SENT**: The message has been sent but not yet confirmed as delivered by the carrier.
- **DELIVERED**: The message has been successfully delivered to the recipient by the carrier.

The following statuses indicate a potentially transient failure condition. Resending a message with the same parameters after some time may result in a successful delivery.

- **SENDING_FAILED**: The message could not be sent.
- **DELIVERY_FAILED**: The message could not be delivered.
- **UNCONFIRMED**: The delivery status of the message is unconfirmed by the carrier.
- **UNDELIVERED**: The message was not delivered. (Handset is turned off, out of coverage, etc.)
- **QUEUING_FAILED**: The message could not be queued.


The following statuses indicate a permanent failure condition. Resending the message is unlikely to result in a successful delivery:

- **DELIVERY_REJECTED**: The delivery of the message was rejected by the carrier.
- **INVALID_NUMBER**: The recipient phone number is invalid.
- **STOPPED_NUMBER**: The recipient phone number has been stopped.
- **LANDLINE**: The recipient phone number is a landline and cannot receive messages.
- **SEND_REJECTED**: The sending of the message was rejected.


#### Example Webhook Payload for Message Status Update

```json
{
  "event_type": "MESSAGE_STATUS",
  "occurred_at": "2024-06-01T12:00:00Z",
  "recipient_phone_number": "+1234567890",
  "sending_phone_number": "+1987654321",
  "message_id": "063b76d1-3713-47dc-92c7-73adf2f7ef40",
  "message_status": "DELIVERED",
  "message_status_details": "Message successfully delivered",
  "custom_metadata": "External custom ID"
}
```

#### Example Webhook Payload for Incoming Message Notification

```json
{
  "event_type": "INCOMING_MESSAGE",
  "occurred_at": "2024-06-01T12:00:00Z",
  "recipient_phone_number": "+1234567890",
  "sending_phone_number": "+1987654321",
  "incoming_message_body": "Sample message text"
}
```

### Webhook Signature Verification

For security purposes, webhook POST requests include two headers:

- `x-leafbuyer-signature`: This header contains the HMAC SHA256 signature.
- `x-leafbuyer-timestamp`: This header contains the timestamp of when the webhook was generated.

The signature is generated using the following components:

- **Secret API Key**: Your `Client Secret` key.
- **Timestamp**: The current UNIX timestamp.
- **Webhook URI**: The URI to which the webhook is sent.
- **Payload**: The JSON payload of the webhook.

#### How to Verify the Signature

To verify the authenticity of the webhook signature, follow these steps:

1. **Concatenate** the timestamp, webhook URI, and payload into a single string.
2. **Compute the HMAC SHA256** hash of the concatenated string using your `Client Secret` key.
3. **Compare** the computed hash with the value in the `x-leafbuyer-signature` header.

To guard against replay attacks, ensure the timestamp is within an acceptable time window (e.g., the last 5 minutes) of the current time.

#### Signature Verification

Given the following input parameters:

- **Secret Key**: `CsrVIa2Q88KN92ntjVdF9TKHOWgZ3X4Cp5hDTBj7zaedg4CarmBzy0oENb5XkQHJ`
- **Timestamp**: `1719974917`
- **Webhook URL**: `http://localhost:8069`
- **Payload**: `{"event_type":"INCOMING_MESSAGE","occurred_at":"2024-07-01T23:43:57.971Z","recipient_phone_number":"+16075759134","sending_phone_number":"+14026744305","incoming_message_body":"This is a simple message"}`

The expected signature is:

`d23a43c5d2b612657f34cd2a3f271b62dd6eb1d40c20895e830ebb220670a39c`

**Important note:** If you are not getting the same request signature as the signature header, please ensure that you are including the linefeed character (`'\n'`) that appears at the end the payload when computing the signature.

## Recipient Opt-Out Management

To Be Completed


# Texting API Reference

### Obtaining the list of Locations

The following query returns a list of all locations present in your tenant (Account).

**Query:**

```graphql
{
  tenantLocations {
    tenantId
    locationId
    name
  }
}
```

A sample response would be:

```
{
	"data": {
		"tenantLocations": [
			{
				"tenantId": "5a7251b0-a78f-4ff5-9de6-bc967867a73d",
				"locationId": "757fe4f8-8e35-474d-b20a-31752cbfed88",
				"name": "location A"
			},
			{
				"tenantId": "5a7251b0-a78f-4ff5-9de6-bc967867a73d",
				"locationId": "0b60b62d-ce34-4675-becd-edb9e0ee342a",
				"name": "Location B"
			}
		]
	}
}
```

### Creating Customers

**createCustomer Mutation:**

Creates a new customer associated with a specific location.

**Input:**

```graphql
input CustomerInput {
  phoneNumber: String
  firstName: String
  lastName: String
  email: String
  zipcode: String
  birthday: String
}
```

**Mutation:**

```graphql
mutation {
  createCustomer(locationId: ID!, customer: CustomerInput): Customer
}
```

**Example Request:**

```graphql
mutation {
  createCustomer(locationId: "LOCATION_ID", customer: {
    phoneNumber: "+1234567890",
    firstName: "John",
    lastName: "Doe",
    email: "john.doe@example.com",
    zipcode: "12345",
    birthday: "1990-01-01",
    interestIds: ["INTEREST_ID_1", "INTEREST_ID_2"],
    usageTypeIds: ["USAGE_TYPE_ID_1", "USAGE_TYPE_ID_2"]
  }) {
    id
    phoneNumber
    firstName
    lastName
    email
    zipcode
    birthday
    interests {
      id
      name
    }
    usageTypes {
      id
      name
    }
  }
}
```

### Creating Campaigns


### createGroupCampaign Mutation

Creates a new group campaign.

**Input:**

```graphql
input GroupCampaignInput {
  locationIds: [ID]!
  messageBody: String!
  scheduledAt: DateTime
  textMessageType: textMessageType
  deliveryMethod: CampaignDeliveryMethod
  }
```

**Mutation:**

```graphql
mutation {
  createGroupCampaign(campaign: GroupCampaignInput): GroupCampaign
}
```

**Example Request:**

```graphql
mutation {
  createGroupCampaign(campaign: {
    locationIds: ["LOCATION_ID_1", "LOCATION_ID_2"],
    messageBody: "Don't miss our summer sale!",
    scheduledAt: "2024-06-01T10:00:00Z",
    textMessageType: SMS,
    deliveryMethod: REGULAR_MESSAGE_SETTING,
  }) {
    id
    locationIds
    messageBody
    scheduledAt
    status
  }
}
```

### cancelGroupCampaign Mutation

Cancels an existing group campaign.

**Mutation:**

```graphql
mutation {
  cancelGroupCampaign(campaignId: ID!): GroupCampaign
}
```

**Example Request:**

```graphql
mutation {
  cancelGroupCampaign(campaignId: "CAMPAIGN_ID") {
    id
    status
  }
}
```

### updateGroupCampaign Mutation

Updates an existing group campaign.

**Input:**

```graphql
input GroupCampaignInput {
  locationIds: [ID]!
  messageBody: String!
  scheduledAt: DateTime
  textMessageType: textMessageType
  deliveryMethod: CampaignDeliveryMethod
}
```

**Mutation:**

```graphql
mutation {
  updateGroupCampaign(locationIds: [ID]!, campaignId: ID!, campaign: GroupCampaignInput): GroupCampaign
}
```

**Example Request:**

```graphql
mutation {
  updateGroupCampaign(locationIds: ["LOCATION_ID_1"], campaignId: "CAMPAIGN_ID", campaign: {
    messageBody: "Updated campaign message",
    scheduledAt: "2024-06-01T10:00:00Z",
    textMessageType: SMS,
    deliveryMethod: IMAGE_ONLY_SETTING,
  }) {
    id
    locationIds
    messageBody
    scheduledAt
    status
  }
}
```



## Partial API Reference

## Enums and Types

### textMessageType

An enum representing the type of text message.

- **UNKNOWN_TEXT_MESSAGE_TYPE**
- **SMS**
- **MMS**

### GroupCampaignInput

The input type for creating or updating a group campaign.

- **locationIds**: The IDs of the locations associated with the campaign. ([ID]!)
- **messageBody**: The body of the message to be sent. (String!)
- **trackingLinkType**: The type of tracking link associated with the campaign. (TrackingLinkType)
- **scheduledAt**: The date and time the campaign is scheduled to be sent. (DateTime)
- **interestIds**: The IDs of the interests associated with the campaign. ([String])
- **usageTypeIds**: The IDs of the usage types associated with the campaign. ([String])
- **carrierTypeIds**: The IDs of the carrier types associated with the campaign. ([String])
- **imageId**: The ID of the image associated with the campaign. (ID)
- **textMessageType**: The type of text message. (textMessageType)
- **deliveryMethod**: The delivery method for the campaign. (CampaignDeliveryMethod)
- **messageCenterEnvelopeTemplateId**: The ID of the message center envelope template. (String)
- **campaignSegmentation**: The segmentation details for the campaign. (CampaignSegmentationInput)
- **addSmartappAdvertising**: Indicates if Smartapp advertising is added. (Boolean!)
- **includeLinktreeUrl**: Indicates if the Linktree URL is included. (Boolean!)
- **includeInLbTotalNetwork**: Indicates if the campaign is included in the LB Total Network. (Boolean!)
- **lbTotalNetworkExpirationDate**: The expiration date for the LB Total Network inclusion. (DateTime)
- **bigDataTracking**: The big data tracking details for the campaign. (CampaignBigDataTrackingInput)
- **mmsImageUrl**: The URL of the MMS image. (String)
- **estimatedCreditCost**: The estimated credit

 cost for the campaign. (Int)
- **instantAction**: The instant action details for the campaign. (CampaignInstantActionInput)

### GroupCampaign

Represents a group campaign.

- **id**: The unique identifier of the campaign. (ID!)
- **locationIds**: The IDs of the locations associated with the campaign. ([ID]!)
- **messageBody**: The body of the message to be sent. (String!)
- **trackingLinkType**: The type of tracking link associated with the campaign. (TrackingLinkType)
- **createdOn**: The date and time the campaign was created. (DateTime)
- **scheduledAt**: The date and time the campaign is scheduled to be sent. (DateTime)
- **sentAt**: The date and time the campaign was sent. (DateTime)
- **status**: The current status of the campaign. (String)
- **interestIds**: The IDs of the interests associated with the campaign. ([String])
- **usageTypeIds**: The IDs of the usage types associated with the campaign. ([String])
- **carrierTypes**: The types of carriers associated with the campaign. ([CarrierType])
- **campaignSegmentation**: The segmentation details for the campaign. (CampaignSegmentation)
- **reachAtCreation**: The reach of the campaign at the time of creation. (Int)
- **reachAtExecution**: The reach of the campaign at the time of execution. (Int)
- **messageSuccessDeliveryCount**: The count of successfully delivered messages. (Int)
- **messageFailureDeliveryCount**: The count of failed message deliveries. (Int)
- **trackingLinkOpenedCount**: The count of tracking link opens. (Int)
- **imageId**: The ID of the image associated with the campaign. (ID)
- **textMessageType**: The type of text message. (textMessageType)
- **creditsCharged**: The number of credits charged for the campaign. (Int)
- **executionMemo**: The memo for the campaign execution. (String)
- **deliveryMethod**: The delivery method for the campaign. (CampaignDeliveryMethod)
- **messageCenterEnvelopeTemplateId**: The ID of the message center envelope template. (String)
- **addSmartappAdvertising**: Indicates if Smartapp advertising is added. (Boolean!)
- **includeLinktreeUrl**: Indicates if the Linktree URL is included. (Boolean!)
- **includeInLbTotalNetwork**: Indicates if the campaign is included in the LB Total Network. (Boolean!)
- **lbTotalNetworkExpirationDate**: The expiration date for the LB Total Network inclusion. (DateTime)
- **bigDataTracking**: The big data tracking details for the campaign. (CampaignBigDataTracking)
- **mmsImageUrl**: The URL of the MMS image. (String)
- **estimatedCreditCost**: The estimated credit cost for the campaign. (Int)
- **instantAction**: The instant action details for the campaign. (CampaignInstantAction)



